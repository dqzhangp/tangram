#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=.

PKG_NAME="tangram"

which gnome-autogen.sh || {
    echo "You need to install gnome-common from the GNOME CVS"
    exit 1
}

USE_GNOME2_MACROS=1 . gnome-autogen.sh
