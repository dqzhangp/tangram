#include <gtk/gtk.h>
#include <webkit/webkit.h>
#include <JavaScriptCore/JavaScript.h>

#include "netconf.h"

void
tangram_netconf ()
{
	GtkWidget* netconf_dialog;
	WebKitWebView* netconf_view;
	WebKitWebFrame* netconf_frame;
	JSGlobalContextRef netconf_context;
	JSClassRef netconf_jsclass;
	JSObjectRef netconf_jsobject;
	JSClassDefinition NetconfObject_definition = kJSClassDefinitionEmpty;

	netconf_dialog = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	netconf_view = WEBKIT_WEB_VIEW (webkit_web_view_new ());
	gtk_container_add (GTK_CONTAINER (netconf_dialog), GTK_WIDGET (netconf_view));
	webkit_web_view_load_uri (WEBKIT_WEB_VIEW (netconf_view), NETCONF_VIEW_URI);

	gtk_window_set_default_size (GTK_WINDOW (netconf_dialog), 480, 320);
	gtk_window_set_title (GTK_WINDOW (netconf_dialog), "网络配置");
	gtk_window_set_position (GTK_WINDOW (netconf_dialog), GTK_WIN_POS_CENTER);
	gtk_window_set_modal (GTK_WINDOW (netconf_dialog), TRUE);

	netconf_frame = webkit_web_view_get_main_frame (WEBKIT_WEB_VIEW (netconf_view));
	netconf_context = webkit_web_frame_get_global_context (WEBKIT_WEB_FRAME (netconf_frame));
	netconf_jsclass = JSClassCreate (&NetconfObject_definition);
	netconf_jsobject = JSObjectMake (netconf_context, netconf_jsclass, NULL);
	JSObjectRef global_jsobject = JSContextGetGlobalObject (netconf_context);
	JSObjectSetProperty (netconf_context, global_jsobject, JSStringCreateWithUTF8CString ("NetConf"), netconf_jsobject, kJSPropertyAttributeNone, NULL);

	{
		JSValueRef value;
		JSStringRef name;

		name = JSStringCreateWithUTF8CString ("teststr");
		value = JSValueMakeString (netconf_context, JSStringCreateWithUTF8CString ("netconf1.2.3.4"));
		JSObjectSetProperty (netconf_context, global_jsobject, name, value, kJSPropertyAttributeNone, NULL);
	}

	gtk_widget_show_all (netconf_dialog);
}
